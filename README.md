# CloudTrail.js
JavaScript base CloudTrail log viewer & *processor* using AWS SNS/SQS/S3/DynamoDB via AWS SDK for Javascript
# usage
You need to setup SQS queue to recieve message and DynamoDB for storage.
You also need to set localStorage accountId,accessKeyId,secretAccessKey with required IAM policy (until login form is ready).
# TODO
- login form
- better search (maybe using Global Secondary Index?)
- error handling
- etc...
