function gzip2blobstr(buf)
{
	var i;
	var ret="";
	
	// zlib const
	var Z_DEFLATED = 8;
	var RESERVED = 224;
	var EXTRA_FIELD = 4;
	
	if( ( buf[0] != 31 ) || ( buf[1] != 139 ) )
	{
		return; // not a gzip file
	}
	i=2;
	var method = buf[i++];
	var flags  = buf[i++];
	
	if (method != Z_DEFLATED || (flags & RESERVED) != 0) {
		return; // mode error
	}
	i+=6;
	for(j=i;j<buf.length;j++)
	{
//		console.log(String.fromCharCode(buf[j]));
		ret+=String.fromCharCode(buf[j]);
	}
	return ret;
}
function processMsg()
{
	var start = new Date();
	queue.receiveMessage({MaxNumberOfMessages:1},function (err, data) {
		if (data.Messages)
		{
			console.log(data);
			for (i=0 ; i<data.Messages.length ; i++)
			{
				var body = eval('(' + data.Messages[i].Body + ')');
				var message = eval('(' + body['Message'] + ')');
				var records;
				if(message['s3ObjectKey'].length>0)
				{
					for(j=0;j<message['s3ObjectKey'].length;j++)
					{
//						console.log(message['s3Bucket']+'/'+message['s3ObjectKey'][j])
						var bucket = new AWS.S3({params: {Bucket: message['s3Bucket']}});
						bucket.getObject({Key: message['s3ObjectKey'][j]}, function(err,data){
							records=JSON.parse(zip_inflate(gzip2blobstr(data.Body))).Records;
							for(k=0;k<records.length;k++)
							{
								console.log(records[k]);
								record=records[k];
								var region_service=record.awsRegion+"_"+record.eventSource;
								var timestamp=new Date(record.eventTime)*1+k;
								var json=JSON.stringify(record);
//								console.log(region_service,timestamp,json);
//								$('#log').val($('#log').val()+[region_service,timestamp,json].join(',')+"\n");
								var itemParams = {Item:{
									s:{S:region_service},
									t:{N:timestamp.toString()},
									a:{S:record.eventName},
									i:{S:record.sourceIPAddress},
									b:{S:json}
								}};
								table.putItem(itemParams, function(err,data) {if (err){console.log(err);}});
							}
						});
					}
				}
				queue.deleteMessage( { ReceiptHandle : data.Messages[i].ReceiptHandle } ,function(err){if(err){console.log(err);}});
			}
		}
	});
	setTimeout(processMsg,interval*1000);
}

function populate_select(id,array)
{
	var options='<option value="">- SELECT -</option>';
	for(var i=0; i<array.length;i++)
	{
		options+='<option>'+array[i]+'</option>';
	}
	$('#'+id).html(options);
}
function a2tr(array,style)
{
	var tr;
	if(style)
	{
		tr='<tr class="'+style+'">';		
	}
	else
	{
		tr='<tr>';		
	}
	for(var i=0;i<array.length;i++)
	{
		tr+='<td class="field'+i+'">'+array[i]+"</td>";
	}
	tr+="</tr>";
	return tr;
}


function search(){
	$('#result').html('');
	$('#filter').prop('checked',false);
	region=$('#region').val();
	service=$('#service').val();
//	region_service = $('#region').val() + '_' + $('#service').val() + ".amazonaws.com";
	filter={};
	if( region != "")
	{
		if( service != "")
		{
			filter['s'] = {"ComparisonOperator":"EQ","AttributeValueList":[{S:region+"_"+service+".amazonaws.com" }]};
		}
		else
		{
			filter['s'] = {"ComparisonOperator":"BEGINS_WITH","AttributeValueList":[{S:region}]};
		}
	}
	if ($('#datefrom').val() && $('#dateto').val())
	{
		timestamp_from = (new Date($('#datefrom').val()) * 1).toString();
		timestamp_to = (new Date($('#dateto').val()) * 1 + 999).toString();
//		console.log(timestamp_from, timestamp_to);
		if (timestamp_from != "" && timestamp_to != "")
		{
			filter['t'] = {"ComparisonOperator": "BETWEEN","AttributeValueList":[{N:timestamp_from},{N:timestamp_to}]}
		};
	}
	params = {ScanFilter: filter , AttributesToGet:["s","t","a","i"]};
	table.scan(params,function(err,data)
	{
//		console.log(params,err,data,data.Items);
		for(var i=0;i<data.Items.length;i++)
		{
			var d=data.Items[i];
			$('#result').append(a2tr([new Date(Number(d.t.N)),d.s.S,d.a.S,d.i.S,'<button class="btn-s" type="button" onclick="details(\''+d.s.S+'\','+d.t.N+')">details</button>'], (d.a.S.match(/^(Describe|List|Get)/)? "readonly":"") ));
		}
	});
}

function details(s,t)
{
//	console.log(s,t);
	table.getItem({
		Key: { s:{S:s}, t:{N:t.toString()} }
	},function(err,data){
		console.log(err,JSON.parse(data.Item.b.S));
		alert(JSON.stringify(JSON.parse(data.Item.b.S)) );
	});
}

function toggle_describe()
{
	trs=document.querySelectorAll('tr.readonly');
	for(var i=0;i<trs.length;i++)
	{
		trs[i].style.display=$('#filter').prop('checked')? 'none':'';
	}
}
